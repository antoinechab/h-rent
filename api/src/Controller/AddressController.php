<?php

namespace App\Controller;

use App\Repository\AddressRepository;
use App\Repository\AnnonceRepository;
use App\Service\GeolocManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AddressController extends AbstractController
{
    public function __construct(private AnnonceRepository $annonceRepository)
    {
    }

    public function getGeolocation(Request $request, AddressRepository $addressRepository, GeolocManager $geolocManager)
    {
        $address = $addressRepository->findOneBy(['id' => $request->get('id')]);
        $json = $geolocManager->getGeoloc($address);

        return $this->json($json);

    }

    public function getAnnonceByGeolocation(Request $request, AddressRepository $addressRepository, GeolocManager $geolocManager)
    {
        try {
            $annonces = $this->annonceRepository->findByGeolocation($request);
        } catch (\Exception $e) {
            return new Response('', Response::HTTP_BAD_REQUEST);
        }
        return $this->json($annonces);
    }
}
