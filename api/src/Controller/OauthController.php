<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class OauthController extends AbstractController
{
    public function __construct(private UserRepository $userRepository, private UserPasswordHasherInterface $passwordHasher)
    {
    }

    #[Route("/oauth/login", name:"oauth_login", methods: ["POST"])]
    public function login(Request $request) {
        $data = json_decode($request->getContent(), true);
        if (!$data) {
            return $this->json([
                'error' => 'invalid_request',
                'error_description' => 'The request body must contain valid JSON.'
            ], 400);
        }
        if (!isset($data['email']) || !isset($data['name']) || !isset($data['id']) || !isset($data['token'])) {
            return $this->json([
                'error' => 'invalid_request',
                'error_description' => 'The request body must contain all of the required parameters.'
            ], 400);
        }
        try {
            $responseToken = $this->register($data);
            return new Response($responseToken, 200);
        } catch (Exception $e) {
            return $this->json([
                'error' => 'internal_server_error',
                'error_description' => 'An internal server error occurred.'
            ], 500);
        }
    }

    private function register(array $data) {

        try {
            $user = $this->userRepository->findOneBy(['email' => $data['email']]);
            if (!$user) {
                $user = new User();
                $user->setEmail($data['email']);
                $user->setPseudo($data['name']);
                if (count($nameExploded = explode(' ', $data['name'])) > 1) {
                    $user->setFirstName($nameExploded[0]);
                    $user->setLastName($nameExploded[1]);
                } else {
                    $user->setFirstName(substr($data['name'], 0, 2));
                    $user->setLastName(substr($data['name'], 2));
                }
                $hash = $this->passwordHasher->hashPassword($user, "1234");
                $user->setPassword($hash);
                $user->setToken($data['token']);
                $user->setActive(true);
                $user->setRoles(["ROLE_USER"]);
                $this->userRepository->save($user);
            }
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $_ENV['SITE_URL'] . '/login',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => json_encode([
                    'email' => $data['email'],
                    'password' => '1234'
                ]),
                CURLOPT_HTTPHEADER => array(
                    'Accept: application/json',
                    'Content-Type: application/json'
                ),
            ));
            return curl_exec($curl);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}
