<?php

namespace App\Controller;

use App\Entity\Booking;
use App\Repository\BookingRepository;
use App\Service\PaypalManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class BookingValidateController extends AbstractController
{
    public function __construct(private BookingRepository $bookingRepository, private PaypalManager $paypalManager)
    {
    }

    /**
     * @throws \Exception
     */
    public function __invoke(Request $request, Booking $data)
    {
        try {
            $this->paypalManager->validatePayment($data);
            $this->bookingRepository->save($data);
            $this->bookingRepository->canceleOtherBooking($data);
        } catch (\Exception $e) {
            return $this->json([
                'error' => $e->getMessage(),
            ], 500);
        }

        return $this->json([
            'message' => 'Booking validated',
        ]);
    }

}
