<?php

namespace App\Controller;

use League\OAuth2\Client\Provider\Github;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GithubTokenController extends AbstractController
{
    #[Route('/oauth/check/github', name: 'github_token')]
    public function index(Request $request, Github $github): JsonResponse
    {
        return $this->json(
            $github->getAccessToken('authorization_code', [
                'code' => $request->request->get('code'),
            ])
        );
    }
}
