<?php

namespace App\EventListener;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Lexik\Bundle\JWTAuthenticationBundle\Events;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class JwtListener implements EventSubscriberInterface
{

    public function __construct(private UserRepository $userRepository, private IriConverterInterface $iriConverter)
    {
    }

    public function onJWTCreated(JWTCreatedEvent $event): void
    {
        $payload = $event->getData();
        $user = $this->userRepository->findOneBy(['email' => $payload['username']]);
        if (!$user) {
            return;
        }
        $payload['@id'] = $this->iriConverter->getIriFromItem($user);
        $payload['id'] = $user?->getId();

        $event->setData($payload);
    }


    public static function getSubscribedEvents()
    {
        return [Events::JWT_CREATED => 'onJWTCreated'];
    }
}
