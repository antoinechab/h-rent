<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Address;
use App\Entity\User;
use App\Service\GeolocManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;

class AddressDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $entityManager, private GeolocManager $geolocManager)
    {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Address;
    }

    /**
     * @param Address $data
     * @param array $context
     * @return Address|object|null
     * @throws \Exception
     */
    public function persist($data, array $context = [])
    {
        $geoloc = json_decode($this->geolocManager->getGeoloc($data), true);

        $data->setLatitude($geoloc['data'][0]['latitude']??null);
        $data->setLongitude($geoloc['data'][0]['longitude']??null);

        $this->entityManager->persist($data);
        $this->entityManager->flush();
        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
