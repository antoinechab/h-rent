<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Booking;
use App\Entity\Payement;
use Doctrine\ORM\EntityManagerInterface;

class PayementDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Payement;
    }

    /**
     * @param Payement $data
     * @param array $context
     * @return Payement|object|null
     * @throws \Exception
     */
    public function persist($data, array $context = [])
    {
        $booking = $data->getBooking();
        if (!$booking) {
            throw new \Exception('Booking not found');
        }
        $booking->setStatut(Booking::STATUS_PAYED);
        $this->entityManager->persist($booking);
        $this->entityManager->persist($data);
        $this->entityManager->flush();
        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
