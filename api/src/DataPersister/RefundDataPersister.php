<?php

namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\Booking;
use App\Entity\Refund;
use App\Repository\BookingRepository;
use App\Repository\PayementRepository;
use App\Service\PaypalManager;
use Doctrine\ORM\EntityManagerInterface;
use HTTP_Request2;
use HTTP_Request2_Exception;

class RefundDataPersister implements ContextAwareDataPersisterInterface
{
    public function __construct(private EntityManagerInterface $entityManager, private PaypalManager $paypalManager, private BookingRepository $bookingRepository)
    {
    }

    public function supports($data, array $context = []): bool
    {
        return $data instanceof Refund;
    }

    /**
     * @param Refund $data
     * @param array $context
     * @return Refund|object|null
     * @throws \Exception
     */
    public function persist($data, array $context = [])
    {
        try {
            $booking = $data->getBooking();
            if (!$booking) {
                throw new \Exception('Booking not found');
            }
            $accessToken = $this->paypalManager->getToken();
            $paypal_response = $this->paypalManager->refund($booking, $accessToken);
            $data->setPaypalResponse(json_decode($paypal_response, true));
            $data->setStatut(Booking::STATUS_REFUNDED);
            $booking->setStatut(Booking::STATUS_REFUNDED);
            $this->bookingRepository->save($booking);

            $this->entityManager->persist($data);
            $this->entityManager->flush();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        return $data;
    }

    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}
