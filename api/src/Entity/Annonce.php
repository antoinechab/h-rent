<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Filter\GeoLocRadiusFilter;
use App\Repository\AnnonceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AnnonceRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
        'get_geolocation'=>[
            'method'=>'GET',
            'path'=>'/annonces/geolocation',
            'controller'=>'App\Controller\AddressController::getAnnonceByGeolocation',
            'openapi_context'=>[
                'summary'=>'Get annonce by geolocation',
                'description'=>'Get annonce by geolocation and radius',
            ]
        ]
    ],
    denormalizationContext: ['groups' => ['annonce:write']],
    normalizationContext: ['groups' => ['annonce:read']]
)]
#[ApiFilter(DateFilter::class, properties: ['slots.startDate', 'slots.endDate'])]
#[ApiFilter(SearchFilter::class, properties: ['capacity' => 'exact','title' => 'ipartial', 'renter.id' => 'exact'])]
#[ApiFilter(GeoLocRadiusFilter::class)]
#[ApiFilter(OrderFilter::class, properties: ['capacity', 'title', 'slots.startDate', 'slots.endDate', 'slots.price'])]
class Annonce
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['annonce:read','annonce:write', 'booking:read', 'files:read', 'slot:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['annonce:read','annonce:write'])]
    private $title;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Groups(['annonce:read','annonce:write'])]
    private $description;

    #[ORM\Column(type: 'integer')]
    #[Groups(['annonce:read','annonce:write'])]
    private $capacity;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'annonces')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['annonce:read','annonce:write'])]
    private $renter;

    #[ORM\OneToMany(mappedBy: 'annonce', targetEntity: Slot::class)]
    #[Groups(['annonce:read'])]
    private $slots;

    #[ORM\OneToOne(inversedBy: 'annonce', targetEntity: Address::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['annonce:read','annonce:write'])]
    private $address;

    #[ORM\OneToMany(mappedBy: 'annonce', targetEntity: Booking::class)]
    #[Groups(['annonce:read'])]
    private $bookings;

    #[ORM\OneToMany(mappedBy: 'annonce', targetEntity: Files::class, cascade: ['persist', 'remove'])]
    #[Groups(['annonce:read'])]
    private $pictures;

    public function __construct()
    {
        $this->slots = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->pictures = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getRenter(): ?User
    {
        return $this->renter;
    }

    public function setRenter(?User $renter): self
    {
        $this->renter = $renter;

        return $this;
    }

    /**
     * @return Collection<int, Slot>
     */
    public function getSlots(): Collection
    {
        return $this->slots;
    }

    public function addSlot(Slot $slot): self
    {
        if (!$this->slots->contains($slot)) {
            $this->slots[] = $slot;
            $slot->setAnnonce($this);
        }

        return $this;
    }

    public function removeSlot(Slot $slot): self
    {
        if ($this->slots->removeElement($slot)) {
            // set the owning side to null (unless already changed)
            if ($slot->getAnnonce() === $this) {
                $slot->setAnnonce(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setAnnonce($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getAnnonce() === $this) {
                $booking->setAnnonce(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Files>
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function addPicture(Files $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setAnnonce($this);
        }

        return $this;
    }

    public function removePicture(Files $picture): self
    {
        if ($this->pictures->removeElement($picture)) {
            // set the owning side to null (unless already changed)
            if ($picture->getAnnonce() === $this) {
                $picture->setAnnonce(null);
            }
        }

        return $this;
    }
}
