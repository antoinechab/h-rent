<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    itemOperations: [
        'get'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ],
        'put'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ],
        'patch'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ],
        'delete'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ]
    ],
    denormalizationContext: ["groups"=>['user:write']],
    normalizationContext: ["groups"=>['user:read']],
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['user:read', 'annonce:read', 'booking:read', 'files:read', 'paypal:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    #[Groups(['user:read','user:write','annonce:read'])]
    private $email;

    #[ORM\Column(type: 'json')]
    #[Groups(['user:read'])]
    private $roles = [];

    #[ORM\Column(type: 'string')]
    private $password;

    #[SerializedName("password")]
    #[Groups(['user:write'])]
    private $plainPassword;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['user:read','user:write','annonce:read'])]
    private $firstName;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['user:read','user:write','annonce:read'])]
    private $lastName;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['user:read','user:write','annonce:read'])]
    private $pseudo;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['user:read'])]
    private $active = true;

    #[ORM\OneToMany(mappedBy: 'renter', targetEntity: Annonce::class, orphanRemoval: true)]
    private $annonces;

    #[ORM\OneToMany(mappedBy: 'tenant', targetEntity: Booking::class, orphanRemoval: true)]
    private $bookings;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Files::class, cascade: ['persist', 'remove'])]
    private $files;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: PaypalInformation::class, orphanRemoval: true)]
    private $paypalInformation;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $token;

    public function __construct()
    {
        $this->annonces = new ArrayCollection();
        $this->bookings = new ArrayCollection();
        $this->files = new ArrayCollection();
        $this->paypalInformation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(?string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * @param mixed $plainPassword
     */
    public function setPlainPassword($plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    public function getSalt()
    {
        return $this->password;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection<int, Annonce>
     */
    public function getAnnonces(): Collection
    {
        return $this->annonces;
    }

    public function addAnnonce(Annonce $annonce): self
    {
        if (!$this->annonces->contains($annonce)) {
            $this->annonces[] = $annonce;
            $annonce->setRenter($this);
        }

        return $this;
    }

    public function removeAnnonce(Annonce $annonce): self
    {
        if ($this->annonces->removeElement($annonce)) {
            // set the owning side to null (unless already changed)
            if ($annonce->getRenter() === $this) {
                $annonce->setRenter(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->setTenant($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            // set the owning side to null (unless already changed)
            if ($booking->getTenant() === $this) {
                $booking->setTenant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Files>
     */
    public function getFiles(): Collection
    {
        return $this->files;
    }

    public function addFile(Files $file): self
    {
        if (!$this->files->contains($file)) {
            $this->files[] = $file;
            $file->setOwner($this);
        }

        return $this;
    }

    public function removeFile(Files $file): self
    {
        if ($this->files->removeElement($file)) {
            // set the owning side to null (unless already changed)
            if ($file->getOwner() === $this) {
                $file->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, PaypalInformation>
     */
    public function getPaypalInformation(): Collection
    {
        return $this->paypalInformation;
    }

    public function addPaypalInformation(PaypalInformation $paypalInformation): self
    {
        if (!$this->paypalInformation->contains($paypalInformation)) {
            $this->paypalInformation[] = $paypalInformation;
            $paypalInformation->setOwner($this);
        }

        return $this;
    }

    public function removePaypalInformation(PaypalInformation $paypalInformation): self
    {
        if ($this->paypalInformation->removeElement($paypalInformation)) {
            // set the owning side to null (unless already changed)
            if ($paypalInformation->getOwner() === $this) {
                $paypalInformation->setOwner(null);
            }
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
