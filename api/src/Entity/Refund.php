<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RefundRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: RefundRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['refund:write']],
    normalizationContext: ['groups' => ['refund:read']]
)]
class Refund
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['refund:read', 'refund:write', 'booking:read'])]
    private $id;

    #[ORM\OneToOne(inversedBy: 'refund', targetEntity: Booking::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['refund:read', 'refund:write'])]
    private $booking;

    #[ORM\Column(type: 'integer')]
    #[Groups(['refund:read', 'refund:write'])]
    private $statut;

    #[ORM\Column(type: 'json')]
    private $paypalResponse = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function setBooking(Booking $booking): self
    {
        $this->booking = $booking;

        return $this;
    }

    public function getStatut(): ?int
    {
        return $this->statut;
    }

    public function setStatut(int $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getPaypalResponse(): ?array
    {
        return $this->paypalResponse;
    }

    public function setPaypalResponse(array $paypalResponse): self
    {
        $this->paypalResponse = $paypalResponse;

        return $this;
    }
}
