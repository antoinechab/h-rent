<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Controller\FileUploadController;
use App\Repository\FilesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @Vich\Uploadable
 */
#[ORM\Entity(repositoryClass: FilesRepository::class)]
#[ApiResource(
    collectionOperations: [
        'get',
        'post' => [
            'method' => 'POST',
            'controller' => FileUploadController::class,
            'deserialize' => false,
            'input_formats' => [
                'multipart' => ['multipart/form-data'],
            ],
        ],
    ],
    itemOperations: [
        "get",
        "post_image"=> [
            'method' => 'POST',
            'path' => '/files/{id}/file',
            'controller' => FileUploadController::class,
            'deserialize' => false,
            'denormalization_context'=> ['groups' => ['files:file']],
            'input_formats' => [
                'multipart' => ['multipart/form-data'],
            ]
        ],
        "put"=>[
            'denormalization_context'=>['groups'=>['files:update']]
        ],
        "patch"=>['denormalization_context'=>['groups'=>['files:update']]],
        "delete",
    ],
    denormalizationContext: ['groups' => ['files:write']],
    normalizationContext: ['groups' => ['files:read']]
)]
#[ApiFilter(BooleanFilter::class, properties: ['principal'])]
class Files
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['files:read', 'files:write', 'annonce:read', 'user:read'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'files')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['files:read', 'files:write', 'files:update'])]
    private $user;

    #[ORM\ManyToOne(targetEntity: Annonce::class, inversedBy: 'pictures')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['files:read', 'files:write', 'files:update'])]
    private $annonce;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['files:read', 'files:write', 'annonce:read', 'user:read', 'files:update'])]
    private $name;

    /**
     * @Vich\UploadableField(mapping="post_files", fileNameProperty="path")
     */
    #[Groups(['files:write','files:file'])]
    public ?File $file = null;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['files:read', 'annonce:read', 'user:read'])]
    private $path;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['files:read', 'annonce:read', 'user:read'])]
    private $type;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['files:read', 'files:write', 'files:update'])]
    private $principal;

    #[ORM\Column(type: 'datetime', nullable: true)]
    #[Groups(['files:read'])]
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAnnonce(): ?Annonce
    {
        return $this->annonce;
    }

    public function setAnnonce(?Annonce $annonce): self
    {
        $this->annonce = $annonce;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPrincipal(): ?bool
    {
        return $this->principal;
    }

    public function setPrincipal(bool $principal): self
    {
        $this->principal = $principal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }

    /**
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @param File|null $file
     */
    public function setFile(?File $file): void
    {
        $this->file = $file;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
