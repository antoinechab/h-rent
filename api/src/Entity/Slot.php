<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\SlotRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: SlotRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['slot:write']],
    normalizationContext: ['groups' => ['slot:read']]
)]
class Slot
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['slot:read', 'slot:write','annonce:read'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: Annonce::class, inversedBy: 'slots')]
    #[Groups(['slot:read', 'slot:write'])]
    private $annonce;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['slot:read', 'slot:write','annonce:read'])]
    private $startDate;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['slot:read', 'slot:write','annonce:read'])]
    private $endDate;

    #[ORM\ManyToMany(targetEntity: Booking::class, mappedBy: 'slots')]
    #[Groups(['slot:read', 'slot:write'])]
    private $bookings;

    #[ORM\Column(type: 'float', nullable: true)]
    #[Groups(['slot:read', 'slot:write', 'annonce:read'])]
    private $price;

    public function __construct()
    {
        $this->bookings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnonce(): ?Annonce
    {
        return $this->annonce;
    }

    public function setAnnonce(?Annonce $annonce): self
    {
        $this->annonce = $annonce;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * @return Collection<int, Booking>
     */
    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function addBooking(Booking $booking): self
    {
        if (!$this->bookings->contains($booking)) {
            $this->bookings[] = $booking;
            $booking->addSlot($this);
        }

        return $this;
    }

    public function removeBooking(Booking $booking): self
    {
        if ($this->bookings->removeElement($booking)) {
            $booking->removeSlot($this);
        }

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
