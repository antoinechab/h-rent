<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\PayementRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PayementRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['payement:write']],
    normalizationContext: ['groups' => ['payement:read']]
)]
class Payement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['payement:read', 'payement:write', 'booking:read'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: Booking::class, inversedBy: 'payements')]
    #[Groups(['payement:read', 'payement:write'])]
    private $booking;

    #[ORM\Column(type: 'json')]
    #[Groups(['payement:read', 'payement:write'])]
    private $paypalResponse = [];

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['payement:read', 'payement:write'])]
    private $statut;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBooking(): ?Booking
    {
        return $this->booking;
    }

    public function setBooking(?Booking $booking): self
    {
        $this->booking = $booking;

        return $this;
    }

    public function getPaypalResponse(): ?array
    {
        return $this->paypalResponse;
    }

    public function setPaypalResponse(array $paypalResponse): self
    {
        $this->paypalResponse = $paypalResponse;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }
}
