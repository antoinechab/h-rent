<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AddressRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AddressRepository::class)]
#[ApiResource(
    itemOperations: [
        'get'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ],
        'put'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ],
        'patch'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ],
        'delete'=>[
            'access_control'=>"is_granted('ROLE_USER') or object == user"
        ],
        'get_geolocation'=>[
            'method'=>'GET',
            'path'=>'/addresses/{id}/geolocation',
            'controller'=>'App\Controller\AddressController::getGeolocation',
            'openapi_context'=>[
                'summary'=>'Get geolocation of an address',
                'description'=>'Get geolocation of an address'
            ]
        ]
    ],
    denormalizationContext: ['groups' => ['address:write']],
    normalizationContext: ['groups' => ['address:read']]
)]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['address:read', 'address:write', 'annonce:read'])]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['address:read', 'address:write', 'annonce:read'])]
    private $address;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['address:read', 'address:write', 'annonce:read'])]
    private $additionalAddress;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['address:read', 'address:write', 'annonce:read'])]
    private $city;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['address:read', 'address:write', 'annonce:read'])]
    private $zipCode;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['address:read', 'address:write', 'annonce:read'])]
    private $country;

    #[ORM\OneToOne(mappedBy: 'address', targetEntity: Annonce::class, cascade: ['persist', 'remove'])]
    #[Groups(['address:read'])]
    private $annonce;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['address:read', 'annonce:read'])]
    private $latitude;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    #[Groups(['address:read', 'annonce:read'])]
    private $longitude;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAdditionalAddress(): ?string
    {
        return $this->additionalAddress;
    }

    public function setAdditionalAddress(?string $additionalAddress): self
    {
        $this->additionalAddress = $additionalAddress;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getAnnonce(): ?Annonce
    {
        return $this->annonce;
    }

    public function setAnnonce(Annonce $annonce): self
    {
        // set the owning side of the relation if necessary
        if ($annonce->getAddress() !== $this) {
            $annonce->setAddress($this);
        }

        $this->annonce = $annonce;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }
}
