<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\BookingValidateController;
use App\Repository\BookingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: BookingRepository::class)]
#[ApiResource(
    itemOperations: [
        "get",
        "put",
        "bookings_validate" => [
            "method" => "PUT",
            "path" => "/bookings/{id}/validate",
            "controller" => BookingValidateController::class,
            'denormalization_context'=>['groups'=>['booking:update']],
            "openapi_context" => [
                "summary" => "Validate a booking",
                "description" => "Validate a booking",
                "responses" => [
                    "200" => [
                        "description" => "Booking validated",
                        "content" => [
                            "application/json" => [
                                "schema" => [
                                    "type" => "object",
                                    "properties" => [
                                        "status" => [
                                            "type" => "string",
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
        "patch",
        "delete",
    ],
    denormalizationContext: ['groups' => ['booking:write']],
    normalizationContext: ['groups' => ['booking:read']]
)]
#[ApiFilter(SearchFilter::class, properties: ['startDate', 'endDate', 'status', 'tenant.id'])]
class Booking
{
    const STATUS_UNPAYED = 0;
    const STATUS_PAYED = 1;
    const STATUS_CANCELED = 3;
    const STATUS_REFUNDED = 4;
    const STATUS_VALIDATED = 5;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['booking:read', 'booking:write', 'annonce:read', 'payement:read', 'refund:read', 'slot:read'])]
    private $id;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['booking:read', 'booking:write', 'annonce:read'])]
    private $createdAt;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['booking:read', 'booking:write', 'annonce:read'])]
    private $startAt;

    #[ORM\Column(type: 'integer')]
    #[Groups(['booking:read', 'booking:write', 'annonce:read'])]
    private $nbPeople;

    //0 unpaid; 1 paid; 2 annuled; 3 refund
    #[ORM\Column(type: 'integer')]
    #[Groups(['booking:read', 'booking:write', 'annonce:read'])]
    private $statut;

    #[ORM\ManyToMany(targetEntity: Slot::class, inversedBy: 'bookings')]
    #[Groups(['booking:read', 'booking:write', 'annonce:read'])]
    private $slots;

    #[ORM\ManyToOne(targetEntity: Annonce::class, inversedBy: 'bookings')]
    #[Groups(['booking:read', 'booking:write'])]
    private $annonce;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'bookings')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['booking:read', 'booking:write'])]
    private $tenant;

    #[ORM\OneToOne(mappedBy: 'booking', targetEntity: Refund::class, cascade: ['persist', 'remove'])]
    #[Groups(['booking:read'])]
    private $refund;

    #[ORM\OneToMany(mappedBy: 'booking', targetEntity: Payement::class)]
    #[Groups(['booking:read'])]
    private $payements;

    public function __construct()
    {
        $this->slots = new ArrayCollection();
        $this->payements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStartAt(): ?\DateTimeInterface
    {
        return $this->startAt;
    }

    public function setStartAt(\DateTimeInterface $startAt): self
    {
        $this->startAt = $startAt;

        return $this;
    }

    public function getNbPeople(): ?int
    {
        return $this->nbPeople;
    }

    public function setNbPeople(int $nbPeople): self
    {
        $this->nbPeople = $nbPeople;

        return $this;
    }

    public function getStatut(): ?int
    {
        return $this->statut;
    }

    public function setStatut(int $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return Collection<int, Slot>
     */
    public function getSlots(): Collection
    {
        return $this->slots;
    }

    public function addSlot(Slot $slot): self
    {
        if (!$this->slots->contains($slot)) {
            $this->slots[] = $slot;
        }

        return $this;
    }

    public function removeSlot(Slot $slot): self
    {
        $this->slots->removeElement($slot);

        return $this;
    }

    public function getAnnonce(): ?Annonce
    {
        return $this->annonce;
    }

    public function setAnnonce(?Annonce $annonce): self
    {
        $this->annonce = $annonce;

        return $this;
    }

    public function getTenant(): ?User
    {
        return $this->tenant;
    }

    public function setTenant(?User $tenant): self
    {
        $this->tenant = $tenant;

        return $this;
    }

    public function getRefund(): ?Refund
    {
        return $this->refund;
    }

    public function setRefund(Refund $refund): self
    {
        // set the owning side of the relation if necessary
        if ($refund->getBooking() !== $this) {
            $refund->setBooking($this);
        }

        $this->refund = $refund;

        return $this;
    }

    /**
     * @return Collection<int, Payement>
     */
    public function getPayements(): Collection
    {
        return $this->payements;
    }

    public function addPayement(Payement $payement): self
    {
        if (!$this->payements->contains($payement)) {
            $this->payements[] = $payement;
            $payement->setBooking($this);
        }

        return $this;
    }

    public function removePayement(Payement $payement): self
    {
        if ($this->payements->removeElement($payement)) {
            // set the owning side to null (unless already changed)
            if ($payement->getBooking() === $this) {
                $payement->setBooking(null);
            }
        }

        return $this;
    }
}
