<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\PaypalInformationRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: PaypalInformationRepository::class)]
#[ApiResource(
    denormalizationContext: ['groups' => ['paypal:write']],
    normalizationContext: ['groups' => ['paypal:read']]
)]
#[apiFilter(SearchFilter::class, properties: ['owner.id'])]
class PaypalInformation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['paypal:read', 'paypal:write'])]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'paypalInformation')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['paypal:read', 'paypal:write'])]
    private $owner;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['paypal:read', 'paypal:write'])]
    private $email;

    #[ORM\Column(type: 'boolean')]
    #[Groups(['paypal:read', 'paypal:write'])]
    private $principal;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPrincipal(): ?bool
    {
        return $this->principal;
    }

    public function setPrincipal(bool $principal): self
    {
        $this->principal = $principal;

        return $this;
    }
}
