<?php

namespace App\DataFixtures;

use App\Entity\Annonce;
use App\Entity\Slot;
use App\Repository\AnnonceRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class SlotFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function __construct(private AnnonceRepository $annonceRepository)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $allAnnonces = $this->annonceRepository->findAll();
        foreach ($allAnnonces as $annonce) {
            for ($i = 0; $i < 30; $i++) {
                $slot = new Slot();
                $slot->setAnnonce($annonce);
                $startDate = $faker->dateTimeBetween('1 years', '+1 years');
                $startDate->setTime(10, 0, 0);
                $endDate = clone $startDate;
                $endDate->modify('+1 day');
                $endDate->setTime(9, 0, 0);
                $slot->setStartDate($startDate);
                $slot->setEndDate($endDate);
                $manager->persist($slot);
            }
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['slot','app'];
    }

    public function getDependencies(): array
    {
        return [
            AnnonceFixtures::class,
        ];
    }
}
