<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Annonce;
use App\Repository\AddressRepository;
use App\Repository\UserRepository;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AnnonceFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    public function __construct(private UserRepository $userRepository, private AddressRepository $addressRepository)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $users = $this->userRepository->findAll();
        $addresses = $this->addressRepository->findAll();
        for ($i = 0; $i < 15; $i++) {
            try {
                $annonce = new Annonce();
                $annonce->setTitle("Annonce n°$i")
                    ->setDescription("lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                    ->setPrice(mt_rand(40, 200))
                    ->setCapacity(mt_rand(1, 5));
                $annonce->setRenter(array_shift($users));
                $annonce->setAddress(array_shift($addresses));
                $manager->persist($annonce);
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
            }
        }
        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['annonce', 'app'];
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            AddressFixtures::class,
        ];
    }
}
