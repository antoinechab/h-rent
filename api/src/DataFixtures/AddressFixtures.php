<?php

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AddressFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < 15; $i++) {
            $address = new Address();
            $address->setAddress($faker->streetAddress)
                ->setCity($faker->city)
                ->setZipCode($faker->postcode)
                ->setCountry($faker->country);
            $manager->persist($address);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['address', 'app'];
    }
}
