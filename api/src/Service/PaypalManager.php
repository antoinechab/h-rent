<?php

namespace App\Service;

use App\Entity\Booking;
use App\Repository\PayementRepository;
use App\Repository\PaypalInformationRepository;
use Exception;
use HTTP_Request2;
use HTTP_Request2_Exception;

class PaypalManager
{
    public function __construct(private PayementRepository $payementRepository, private PaypalInformationRepository $paypalInformationRepository)
    {
    }


    /**
     * @throws Exception
     */
    public function getToken(){
        $request = new HTTP_Request2();
        $bearer = base64_encode($_ENV['PAYPAL_USER'].":".$_ENV['PAYPAL_MDP']);
        $request->setUrl($_ENV['PAYPAL_URL'].'/v1/oauth2/token');
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => TRUE
        ));
        $request->setHeader(array(
            'Accept' => 'application/json',
            'Accept-Language' => 'en_US',
            'Authorization' => "Basic $bearer",
            'Content-Type' => 'application/x-www-form-urlencoded'
        ));
        $request->addPostParameter(array(
            'grant_type' => 'client_credentials'
        ));
        try {
            $responseToken = $request->send();
            if ($responseToken->getStatus() != 200) {
                throw new Exception('Unexpected HTTP status: ' . $responseToken->getStatus() . ' ' . $responseToken->getReasonPhrase());
            }
        } catch (HTTP_Request2_Exception $e) {
            throw new Exception('Error: ' . $e->getMessage());
        }
        $body = json_decode($responseToken->getBody(), true);
        return $body['access_token'];
    }

    /**
*     * @throws Exception
     */
    public function refund(Booking $booking, string $accessToken)
    {
        $payement = $this->payementRepository->findOneBy(['booking' => $booking, 'statut' => (string)Booking::STATUS_PAYED]);
        if ($payement === null) {
            throw new Exception('Payement not found or not payed');
        }
        $payementId = $payement->getPaypalResponse()['purchase_units'][0]['payments']['captures'][0]['id'];
        $request = new HTTP_Request2();
        $request->setUrl($_ENV['PAYPAL_URL']."/v1/payments/sale/$payementId/refund");
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => TRUE
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/json',
            'Authorization' => "bearer $accessToken"
        ));
        $request->setBody('{}');
        try {
            $responseRefund = $request->send();
            if (in_array($responseRefund->getStatus(), array(200, 201))) {
                $payement->setStatut(Booking::STATUS_REFUNDED);
                $this->payementRepository->save($payement);
                return $responseRefund->getBody();
            } else {
                throw new Exception('Unexpected HTTP status: ' . $responseRefund->getStatus() . ' ' . $responseRefund->getReasonPhrase());
            }
        } catch (HTTP_Request2_Exception $e) {
            throw new Exception('Error: ' . $e->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function validatePayment(Booking $data): Booking
    {
        $payement = $this->payementRepository->findOneBy(['booking' => $data, 'statut' => (string)Booking::STATUS_PAYED]);
        if ($payement === null) {
            throw new Exception('Payement not found or not payed');
        }

        $accessToken = $this->getToken();
        $amount = $payement->getPaypalResponse()['purchase_units'][0]['payments']['captures'][0]['amount']['value'];
        $amountToSend = $amount - ($amount * (3 / 100));
        $currency = $payement->getPaypalResponse()['purchase_units'][0]['payments']['captures'][0]['amount']['currency_code'];
        $senderId = $payement->getPaypalResponse()['id'];
        $payerId = $payement->getPaypalResponse()['payer']['payer_id'];

        $userPaypalInformation = $this->paypalInformationRepository->findOneBy(['owner' => $data->getTenant(), 'principal' => true]);
        if ($userPaypalInformation === null) {
            throw new Exception('User paypal information not found');
        }
        $receiverEmail = $userPaypalInformation->getEmail();

        $request = new HTTP_Request2();
        $request->setUrl($_ENV['PAYPAL_URL'].'/v1/payments/payouts');
        $request->setMethod(HTTP_Request2::METHOD_POST);
        $request->setConfig(array(
            'follow_redirects' => TRUE
        ));
        $request->setHeader(array(
            'Content-Type' => 'application/json',
            'Authorization' => "bearer $accessToken"
        ));
        $request->setBody(json_encode([
            'sender_batch_header'=> [
            'sender_batch_id'=> "$senderId",
                'recipient_type'=> 'EMAIL',
                'email_subject'=> 'h-rent payement!',
                'email_message'=> "You received a payment for the booking number: {$data->getId()}. Thanks for using our service!"
                ],
            'items'=> [
                [
                    'amount'=> [
                    'value'=> "$amountToSend",
                    'currency'=> "$currency"
                    ],
                    'sender_item_id'=> "$payerId",
                    'recipient_wallet'=> 'PAYPAL',
                    'receiver'=> "$receiverEmail"
                ]
            ]
        ]));
        try {
            $response = $request->send();
            if (in_array($response->getStatus(), array(200, 201))) {
                $data->setStatut(Booking::STATUS_VALIDATED);
                $payement->setStatut(Booking::STATUS_VALIDATED);
                $this->payementRepository->save($payement);
                return $data;
            }
            else {
                throw new Exception('Unexpected HTTP status: ' . $response->getStatus() . ' ' . $response->getReasonPhrase());
            }
        }
        catch(HTTP_Request2_Exception $e) {
            throw new Exception('Error: ' . $e->getMessage());
        }
    }

}
