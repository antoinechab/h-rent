<?php

namespace App\Service;

use App\Entity\Address;

class GeolocManager
{

    public function getGeoloc(Address $address): bool|string
    {
        $addressString = $address->getAddress() . ' ' . $address->getAdditionalAddress() . ' ' . $address->getZipCode() . ' ' . $address->getCity();
        $queryString = http_build_query([
            'access_key' => $_ENV['GEOLOC_KEY'],
            'query' => $addressString,
            'output' => 'json',
            'limit' => 1,
        ]);

        $ch = curl_init(sprintf('%s?%s', 'http://api.positionstack.com/v1/forward', $queryString));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $json = curl_exec($ch);
        curl_close($ch);

        return $json;
    }

    public function getDistance(){
        //get distance between 2 points
        $lat1 = 45.750000;
        $lon1 = 4.850000;
        $lat2 = 45.750000;
        $lon2 = 4.850000;
        $unit = "K";
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
            return ($miles * 1.609344).' km';
        } else if ($unit == "N") {
            return ($miles * 0.8684).' miles';
        } else {
            return $miles.' miles';
        }
    }

}
