<?php

namespace App\Filter;

use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\AbstractContextAwareFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\PropertyInfo\Type;

class GeoLocRadiusFilter extends AbstractContextAwareFilter
{

    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null)
    {
//        if ($property !== 'geoloc_radius') {
//            return;
//        }
//
//        $addressString = str_replace(["françe","france","fr"],"",strtolower($value)) . ' , France';
//        $queryString = http_build_query([
//            'access_key' => $_ENV['GEOLOC_KEY'],
//            'query' => $addressString,
//            'output' => 'json',
//            'limit' => 1,
//        ]);
//
//        $ch = curl_init(sprintf('%s?%s', 'http://api.positionstack.com/v1/forward', $queryString));
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        $json = curl_exec($ch);
//        curl_close($ch);
//        $geoloc = json_decode($json, true);
//
//        $lat = $geoloc['data'][0]['latitude'];
//        $long = $geoloc['data'][0]['longitude'];
//
//        $conf = new Configuration();
//        $conf->addCustomNumericFunction('cosd', 'Doctrine\ORM\Query\AST\Functions\CosineDegrees');
//        $conf->addCustomNumericFunction('acosd', 'Doctrine\ORM\Query\AST\Functions\AcosDegrees');
//        $conf->addCustomNumericFunction('sind', 'Doctrine\ORM\Query\AST\Functions\SineDegrees');
//        //get all annonces with geoloc in radius
//        $queryBuilder
//            ->leftJoin('o.address', 'a')
//            ->andWhere("((acosd(sind(:lat)*sind(a.latitude::float) + cosd(:lat)*cosd(a.latitude::float) * cosd(( :long - a.longitude::float)))) * 60 * 1.1515) <= 10")
//            ->setParameter("lat",$lat)
//            ->setParameter("lng",$long);
    }

    public function getDescription(string $resourceClass): array
    {
        $description = [];
        $description["geoloc_radius"] = [
            'type' => Type::BUILTIN_TYPE_STRING,
            'required' => false,
            'swagger' => [
                'description' => 'Filter using an address. and return all annonce in radius',
                'example' => 'clermont-ferrand+10',
                'name' => 'geoloc_radius',
                'type' => 'string',
            ],
        ];

        return $description;
    }

    /**
     * Method to find the distance between 2 locations from its coordinates.
     *
     * @return Float Distance in Kilometers.
     */
    private function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {
        $theta = $longitude1 - $longitude2;
        $distance = sin(deg2rad($latitude1)) * sin(deg2rad($latitude2)) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta));

        $distance = acos($distance);
        $distance = rad2deg($distance);
        $distance = $distance * 60 * 1.1515;

        switch($unit)
        {
            case 'Mi': break;
            case 'Km' : $distance = $distance * 1.609344;
        }

        return (round($distance,2));
    }
}
