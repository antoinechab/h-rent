<?php

namespace App\Command;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsCommand(
    name: 'app:create:admin',
    description: 'Add a short description for your command',
)]
class CreateAdminCommand extends Command
{
    public function __construct(private UserRepository $userRepository, private UserPasswordHasherInterface $passwordHasher)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
//        $this
//            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
//        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $io->title('Welcome to the CreateAdminCommand!');

            //ask the new user information
            $firstName = $io->ask('What is your first name?');
            $lastName = $io->ask('What is your last name?');
            $username = $io->ask('What is your username?');
            $password = $io->askHidden('What is your password?');
            $passwordConfirm = $io->askHidden('Confirm your password');
            $passwordTry = 3;
            while ($password !== $passwordConfirm && $passwordTry > 0) {
                $io->warning('Passwords do not match '.$passwordTry.' tries left');
                $passwordConfirm = $io->askHidden('Confirm your password');
                $passwordTry--;
            }
            if ($passwordTry <= 0) {
                $io->error('Too many tries');
                return 1;
            }
            $email = $io->ask('What is your email?');
            $userExist = $this->userRepository->findOneBy(['email' => $email]);
            $emailTry = 3;
            while ($userExist && $emailTry > 0) {
                $io->warning('Email already exist '.$emailTry.' tries left');
                $email = $io->ask('What is your email?');
                $userExist = $this->userRepository->findOneBy(['email' => $email]);
                $emailTry--;
            }
            if ($emailTry <= 0) {
                $io->error('Too many tries');
                return 1;
            }
            $roles = $io->choice('What is your role?', ['ROLE_SUPER_ADMIN', 'ROLE_USER']);

            //create the new user
            $user = new User();
            $user->setPseudo($username);
            $user->setEmail($email);
            $user->setFirstName($firstName);
            $user->setLastName($lastName);
            $user->setRoles([$roles]);
            $user->setActive(true);
            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $password
            );
            $user->setPassword($hashedPassword);

            //save the new user
            $this->userRepository->save($user);

            //display success message
            $io->success('The user has been created!');
            return 0;
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            return 1;
        }
    }
}
