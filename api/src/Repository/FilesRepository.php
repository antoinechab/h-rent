<?php

namespace App\Repository;

use App\Entity\Files;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

/**
 * @method Files|null find($id, $lockMode = null, $lockVersion = null)
 * @method Files|null findOneBy(array $criteria, array $orderBy = null)
 * @method Files[]    findAll()
 * @method Files[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, private UserRepository $userRepository, private AnnonceRepository $annonceRepository)
    {
        parent::__construct($registry, Files::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Files $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Files $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws Exception
     */
    public function create(array $data, mixed $file): Files
    {
        if (!$data['user'] && !$data['annonce'])
            throw new BadRequestException('Veuillez renseigner un utilisateur ou une annonce');

        $user = $data['user'] ? $this->userRepository->find($data['user']) : null;
        if ($data['user'] && $user === null)
            throw new BadRequestException('Utilisateur introuvable');

        $annonce = $data['annonce'] ? $this->annonceRepository->find($data['annonce']) : null;
        if ($data['annonce'] && $annonce === null)
            throw new BadRequestException('Annonce introuvable');

        if ($annonce) {
            $pictures = $annonce->getPictures();
            if (count($pictures) >= 5)
                throw new BadRequestException('Vous avez déjà 5 photos pour cette annonce');
            if ((bool)$data['principal']) {
                foreach ($pictures as $picture) {
                    $picture->setPrincipal(false);
                    $this->_em->persist($picture);
                }
            }
        }

        $files = new Files();
        $files->setName($data['name']);
        $files->setType($file->getClientMimeType());
        $files->setUser($user);
        $files->setAnnonce($annonce);
        $files->setPrincipal((bool)$data['principal']);
        $files->setFile($file);
        $files->setCreatedAt(new DateTime());
        $this->add($files);
        return $files;
    }

    /**
     * @throws Exception
     */
    public function update(Files $files, mixed $file): Files
    {
        $files->setType($file->getClientMimeType());
        $files->setFile($file);
        $files->setCreatedAt(new DateTime());

        $this->add($files);
        return $files;

    }
}
