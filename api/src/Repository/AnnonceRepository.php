<?php

namespace App\Repository;

use App\Entity\Annonce;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method Annonce|null find($id, $lockMode = null, $lockVersion = null)
 * @method Annonce|null findOneBy(array $criteria, array $orderBy = null)
 * @method Annonce[]    findAll()
 * @method Annonce[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnnonceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Annonce::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Annonce $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Annonce $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Annonce[] Returns an array of Annonce objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Annonce
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    /**
     * @throws \Exception
     */
    public function findByGeolocation(Request $request)
    {

        try {
            if (($geoloc_radius = $request->query->get("geoloc_radius")) === null) {
                throw new \Exception("geoloc_radius not found");
            }

            $value = $geoloc_radius;
            $radius = 5;
            if (strpos($geoloc_radius, "+") !== false) {
                $geoloc_radius_exploded = explode("+", $geoloc_radius);
                $value = $geoloc_radius_exploded[0];
                $radius = (int)$geoloc_radius_exploded[1];
            }

            $addressString = str_replace(["françe", "france", "fr"], "", strtolower($value)) . ' , France';
            $queryString = http_build_query([
                'access_key' => $_ENV['GEOLOC_KEY'],
                'query' => $addressString,
                'output' => 'json',
                'limit' => 1,
            ]);
            $ch = curl_init(sprintf('%s?%s', 'http://api.positionstack.com/v1/forward', $queryString));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $json = curl_exec($ch);
            curl_close($ch);
            $geoloc = json_decode($json, true);
            $lat = $geoloc['data'][0]['latitude'];
            $long = $geoloc['data'][0]['longitude'];

            $sql = "SELECT id
            FROM (SELECT *,((acosd(sind({$lat})*sind(latitude::float) + cosd({$lat})*cosd(latitude::float) * cosd(( {$long} - longitude::float)))) * 60 * 1.1515) as distance FROM address) annonce
            WHERE distance <= {$radius}";

            $stmt = $this->_em->getConnection()->prepare($sql);
            $result = $stmt->executeQuery()->fetchAllAssociative();

            $annonces = [];
            foreach ($result as $id) {
                $annonces[] = $id['id'];
            }
        } catch (Exception $e) {
            throw new \Exception($e->getMessage());
        }
        return $annonces;
    }
}
