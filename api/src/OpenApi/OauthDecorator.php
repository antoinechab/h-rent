<?php

namespace App\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\Operation;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\Model\RequestBody;
use ApiPlatform\Core\OpenApi\OpenApi;
use ArrayObject;

class OauthDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated
    ) {}


    public function __invoke(array $context = []): OpenApi
    {
        $openApi = ($this->decorated)($context);
        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Oauth'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'token' => [
                    'type' => 'string',
                    'readOnly' => true,
                ],
            ],
        ]);
        $schemas['Credentials_oauth'] = new ArrayObject([
            'type' => 'object',
            'properties' => [
                'email' => [
                    'type' => 'string',
                    'format' => 'email',
                    'example' => 'johndoe@gmail.com'
                ],
                'name' => [
                    'type' => 'string',
                    'example' => 'John Doe'
                ],
                'id' => [
                    'type' => 'string',
                    'example' => '123456789'
                ],
                'token' => [
                    'type' => 'string',
                ],
            ],
        ]);

        $pathItem = new PathItem(
            ref: 'oauth',
            post: new Operation(
                operationId: 'postOauthTokenItem',
                responses: [
                    '200' => [
                        'description' => 'login with oauth',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Credentials_oauth',
                                ],
                            ],
                        ],
                    ],
                ],
                summary: 'login with oauth',
                requestBody: new RequestBody(
                    description: 'login with oauth',
                    content: new ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials_oauth',
                            ],
                        ],
                    ]),
                ),
            ),
        );
        $openApi->getPaths()->addPath('/oauth/login', $pathItem);

        return $openApi;

    }
}
