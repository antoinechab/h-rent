<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220514091709 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE craue_geo_postalcode_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE craue_geo_postalcode (id INT NOT NULL, country VARCHAR(2) NOT NULL, postal_code VARCHAR(20) NOT NULL, lat NUMERIC(9, 6) NOT NULL, lng NUMERIC(9, 6) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX postal_code_idx ON craue_geo_postalcode (country, postal_code)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE craue_geo_postalcode_id_seq CASCADE');
        $this->addSql('DROP TABLE craue_geo_postalcode');
    }
}
