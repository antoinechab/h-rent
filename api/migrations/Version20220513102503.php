<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513102503 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE address ADD latitude VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE address ADD longitude VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE "user" DROP github_id');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE "user" ADD github_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE address DROP latitude');
        $this->addSql('ALTER TABLE address DROP longitude');
    }
}
