<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220512170039 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE files ADD path VARCHAR(255) NOT NULL DEFAULT \'\/path\'');
        $this->addSql('ALTER TABLE files DROP file');
        $this->addSql('ALTER TABLE files ALTER principal DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE files ADD file TEXT NOT NULL');
        $this->addSql('ALTER TABLE files DROP path');
        $this->addSql('ALTER TABLE files ALTER principal SET DEFAULT \'false\'');
    }
}
