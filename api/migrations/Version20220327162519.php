<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220327162519 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE address_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE annonce_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE booking_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE files_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE payement_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE paypal_information_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE refund_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE slot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE address (id INT NOT NULL, address VARCHAR(255) NOT NULL, additional_address VARCHAR(255) DEFAULT NULL, city VARCHAR(255) NOT NULL, zip_code VARCHAR(255) NOT NULL, country VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE annonce (id INT NOT NULL, renter_id INT NOT NULL, address_id INT NOT NULL, title VARCHAR(255) NOT NULL, description TEXT DEFAULT NULL, capacity INT NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_F65593E5E289A545 ON annonce (renter_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F65593E5F5B7AF75 ON annonce (address_id)');
        $this->addSql('CREATE TABLE booking (id INT NOT NULL, annonce_id INT DEFAULT NULL, tenant_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, start_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, nb_people INT NOT NULL, statut INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E00CEDDE8805AB2F ON booking (annonce_id)');
        $this->addSql('CREATE INDEX IDX_E00CEDDE9033212A ON booking (tenant_id)');
        $this->addSql('CREATE TABLE booking_slot (booking_id INT NOT NULL, slot_id INT NOT NULL, PRIMARY KEY(booking_id, slot_id))');
        $this->addSql('CREATE INDEX IDX_B49F02293301C60 ON booking_slot (booking_id)');
        $this->addSql('CREATE INDEX IDX_B49F022959E5119C ON booking_slot (slot_id)');
        $this->addSql('CREATE TABLE files (id INT NOT NULL, owner_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, file TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_63540597E3C61F9 ON files (owner_id)');
        $this->addSql('CREATE TABLE payement (id INT NOT NULL, booking_id INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B20A78853301C60 ON payement (booking_id)');
        $this->addSql('CREATE TABLE paypal_information (id INT NOT NULL, owner_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D7FBC1CB7E3C61F9 ON paypal_information (owner_id)');
        $this->addSql('CREATE TABLE refund (id INT NOT NULL, booking_id INT NOT NULL, statut INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5B2C14583301C60 ON refund (booking_id)');
        $this->addSql('CREATE TABLE slot (id INT NOT NULL, annonce_id INT DEFAULT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AC0E20678805AB2F ON slot (annonce_id)');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E5E289A545 FOREIGN KEY (renter_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E5F5B7AF75 FOREIGN KEY (address_id) REFERENCES address (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE8805AB2F FOREIGN KEY (annonce_id) REFERENCES annonce (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE booking ADD CONSTRAINT FK_E00CEDDE9033212A FOREIGN KEY (tenant_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE booking_slot ADD CONSTRAINT FK_B49F02293301C60 FOREIGN KEY (booking_id) REFERENCES booking (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE booking_slot ADD CONSTRAINT FK_B49F022959E5119C FOREIGN KEY (slot_id) REFERENCES slot (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE files ADD CONSTRAINT FK_63540597E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE payement ADD CONSTRAINT FK_B20A78853301C60 FOREIGN KEY (booking_id) REFERENCES booking (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE paypal_information ADD CONSTRAINT FK_D7FBC1CB7E3C61F9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE refund ADD CONSTRAINT FK_5B2C14583301C60 FOREIGN KEY (booking_id) REFERENCES booking (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE slot ADD CONSTRAINT FK_AC0E20678805AB2F FOREIGN KEY (annonce_id) REFERENCES annonce (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annonce DROP CONSTRAINT FK_F65593E5F5B7AF75');
        $this->addSql('ALTER TABLE booking DROP CONSTRAINT FK_E00CEDDE8805AB2F');
        $this->addSql('ALTER TABLE slot DROP CONSTRAINT FK_AC0E20678805AB2F');
        $this->addSql('ALTER TABLE booking_slot DROP CONSTRAINT FK_B49F02293301C60');
        $this->addSql('ALTER TABLE payement DROP CONSTRAINT FK_B20A78853301C60');
        $this->addSql('ALTER TABLE refund DROP CONSTRAINT FK_5B2C14583301C60');
        $this->addSql('ALTER TABLE booking_slot DROP CONSTRAINT FK_B49F022959E5119C');
        $this->addSql('DROP SEQUENCE address_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE annonce_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE booking_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE files_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE payement_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE paypal_information_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE refund_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE slot_id_seq CASCADE');
        $this->addSql('DROP TABLE address');
        $this->addSql('DROP TABLE annonce');
        $this->addSql('DROP TABLE booking');
        $this->addSql('DROP TABLE booking_slot');
        $this->addSql('DROP TABLE files');
        $this->addSql('DROP TABLE payement');
        $this->addSql('DROP TABLE paypal_information');
        $this->addSql('DROP TABLE refund');
        $this->addSql('DROP TABLE slot');
    }
}
