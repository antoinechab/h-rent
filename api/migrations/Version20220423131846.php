<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220423131846 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE files DROP CONSTRAINT fk_63540597e3c61f9');
        $this->addSql('DROP INDEX idx_63540597e3c61f9');
        $this->addSql('ALTER TABLE files ADD annonce_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE files ADD type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE files RENAME COLUMN owner_id TO user_id');
        $this->addSql('ALTER TABLE files ADD CONSTRAINT FK_6354059A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE files ADD CONSTRAINT FK_63540598805AB2F FOREIGN KEY (annonce_id) REFERENCES annonce (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_6354059A76ED395 ON files (user_id)');
        $this->addSql('CREATE INDEX IDX_63540598805AB2F ON files (annonce_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE files DROP CONSTRAINT FK_6354059A76ED395');
        $this->addSql('ALTER TABLE files DROP CONSTRAINT FK_63540598805AB2F');
        $this->addSql('DROP INDEX IDX_6354059A76ED395');
        $this->addSql('DROP INDEX IDX_63540598805AB2F');
        $this->addSql('ALTER TABLE files ADD owner_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE files DROP user_id');
        $this->addSql('ALTER TABLE files DROP annonce_id');
        $this->addSql('ALTER TABLE files DROP type');
        $this->addSql('ALTER TABLE files ADD CONSTRAINT fk_63540597e3c61f9 FOREIGN KEY (owner_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_63540597e3c61f9 ON files (owner_id)');
    }
}
