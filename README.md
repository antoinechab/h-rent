# build project
```bash
docker-compose build --pull --no-cache
docker-compose up -d 
```

# access
- localhost
- localhost/docs

# login
- localhost/docs
    - /login
        - test@gmail.com
        - test

```text
une fois le token jwt récupéré, il suffit de le mettre comme ci-dessous pour qu'il soit automatiquement ajouté à toutes les requêtes
```
### cliquer sur Authorize
![image.png](./image.png)

### écrire bearer suivi du token
![image-1.png](./image-1.png)

# bdd
```bash
docker cp prod-exported.sql h-rent_database_1:/prod-exported.sql
docker exec -it h-rent_database_1 /bin/sh

psql -U api-platform api < prod-exported.sql
```
# plus d'info dans le wiki du projet

https://gitlab.com/antoinechab/h-rent/-/wikis/home
