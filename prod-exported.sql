--
-- PostgreSQL database dump
--

-- Dumped from database version 13.5
-- Dumped by pg_dump version 13.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: address; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.address (
    id integer NOT NULL,
    address character varying(255) NOT NULL,
    additional_address character varying(255) DEFAULT NULL::character varying,
    city character varying(255) NOT NULL,
    zip_code character varying(255) NOT NULL,
    country character varying(255) NOT NULL,
    latitude character varying(255) DEFAULT NULL::character varying,
    longitude character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.address OWNER TO "api-platform";

--
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.address_id_seq OWNER TO "api-platform";

--
-- Name: annonce; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.annonce (
    id integer NOT NULL,
    renter_id integer NOT NULL,
    address_id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text,
    capacity integer NOT NULL
);


ALTER TABLE public.annonce OWNER TO "api-platform";

--
-- Name: annonce_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.annonce_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.annonce_id_seq OWNER TO "api-platform";

--
-- Name: booking; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.booking (
    id integer NOT NULL,
    annonce_id integer,
    tenant_id integer NOT NULL,
    created_at timestamp(0) without time zone NOT NULL,
    start_at timestamp(0) without time zone NOT NULL,
    nb_people integer NOT NULL,
    statut integer NOT NULL
);


ALTER TABLE public.booking OWNER TO "api-platform";

--
-- Name: booking_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.booking_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.booking_id_seq OWNER TO "api-platform";

--
-- Name: booking_slot; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.booking_slot (
    booking_id integer NOT NULL,
    slot_id integer NOT NULL
);


ALTER TABLE public.booking_slot OWNER TO "api-platform";

--
-- Name: craue_geo_postalcode_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.craue_geo_postalcode_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.craue_geo_postalcode_id_seq OWNER TO "api-platform";

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO "api-platform";

--
-- Name: files; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.files (
    id integer NOT NULL,
    user_id integer,
    name character varying(255) NOT NULL,
    annonce_id integer,
    type character varying(255) NOT NULL,
    principal boolean NOT NULL,
    path character varying(255) NOT NULL,
    created_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone
);


ALTER TABLE public.files OWNER TO "api-platform";

--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.files_id_seq OWNER TO "api-platform";

--
-- Name: payement; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.payement (
    id integer NOT NULL,
    booking_id integer,
    paypal_response json NOT NULL,
    statut character varying(255) NOT NULL
);


ALTER TABLE public.payement OWNER TO "api-platform";

--
-- Name: payement_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.payement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payement_id_seq OWNER TO "api-platform";

--
-- Name: paypal_information; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.paypal_information (
    id integer NOT NULL,
    owner_id integer NOT NULL,
    email character varying(255) NOT NULL,
    principal boolean NOT NULL
);


ALTER TABLE public.paypal_information OWNER TO "api-platform";

--
-- Name: paypal_information_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.paypal_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.paypal_information_id_seq OWNER TO "api-platform";

--
-- Name: refund; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.refund (
    id integer NOT NULL,
    booking_id integer NOT NULL,
    statut integer NOT NULL,
    paypal_response json NOT NULL
);


ALTER TABLE public.refund OWNER TO "api-platform";

--
-- Name: refund_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.refund_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.refund_id_seq OWNER TO "api-platform";

--
-- Name: slot; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public.slot (
    id integer NOT NULL,
    annonce_id integer,
    start_date timestamp(0) without time zone NOT NULL,
    end_date timestamp(0) without time zone NOT NULL,
    price double precision
);


ALTER TABLE public.slot OWNER TO "api-platform";

--
-- Name: slot_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.slot_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.slot_id_seq OWNER TO "api-platform";

--
-- Name: user; Type: TABLE; Schema: public; Owner: api-platform
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying(180) NOT NULL,
    roles json NOT NULL,
    password character varying(255) NOT NULL,
    first_name character varying(255) NOT NULL,
    last_name character varying(255) NOT NULL,
    pseudo character varying(255) DEFAULT NULL::character varying,
    active boolean NOT NULL,
    token character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public."user" OWNER TO "api-platform";

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: api-platform
--

CREATE SEQUENCE public.user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO "api-platform";

--
-- Data for Name: address; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.address (id, address, additional_address, city, zip_code, country, latitude, longitude) FROM stdin;
16	6, chemin Cordier	\N	Huet	32 017	La Barbad	\N	\N
17	4, avenue de Lefevre	\N	Rousset-sur-Moreno	76 865	Mariannes du Nord (Îles)	\N	\N
18	9, place Françoise Pereira	\N	Leduc	84 794	Lettonie	\N	\N
19	860, avenue Isaac De Sousa	\N	Valentin-sur-Renaud	92 575	Burkina Faso	\N	\N
20	place Faivre	\N	Samsonboeuf	13436	Cook (Îles)	\N	\N
21	rue de Ribeiro	\N	Antoine	18 010	Rwanda	\N	\N
22	7, rue de Pires	\N	Blanchet	12 203	Guinée-Bissau	\N	\N
23	impasse Alphonse Nguyen	\N	Nicolas-sur-Neveu	61742	Bangladesh	\N	\N
24	65, boulevard Odette Auger	\N	ChauvinBourg	54 323	Pakistan	\N	\N
25	26, rue Lemaitre	\N	Malletnec	99 676	Lettonie	\N	\N
26	267, boulevard Astrid Leroux	\N	Lefort	87 592	Myanmar	\N	\N
27	92, rue Etienne	\N	Lebreton	58357	Mali	\N	\N
28	27, impasse Lorraine Guyot	\N	Guillotboeuf	02 294	Roumanie	\N	\N
29	95, place Riviere	\N	Fontaine	11 551	Kiribati	\N	\N
31	22 rue bardoux		clermont-ferrrand	63000	France	45.774278	3.088237
32	22 rue bardoux		Clermont-ferrand	63000	France	45.779700	3.086800
30	31 avenue Aristide Briand	\N	Aurillac	15000	France	44.91758125981209	2.43540719599761
\.


--
-- Data for Name: annonce; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.annonce (id, renter_id, address_id, title, description, capacity) FROM stdin;
1	31	20	Annonce n°0	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	4
2	32	25	Annonce n°1	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	5
3	33	26	Annonce n°2	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1
4	34	27	Annonce n°3	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	5
5	35	17	Annonce n°4	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	4
6	36	18	Annonce n°5	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1
7	37	21	Annonce n°6	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	1
8	38	19	Annonce n°7	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	5
9	39	24	Annonce n°8	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2
10	40	28	Annonce n°9	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	5
11	41	30	Annonce n°10	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	3
12	42	29	Annonce n°11	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	5
13	43	16	Annonce n°12	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	3
14	44	23	Annonce n°13	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	4
15	45	22	Annonce n°14	lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.	2
16	61	31	test	tesssssssssssssssssssssssssssssssssssssssssst	3
18	61	32	test2	tesssssssssssssssssssssssssssssssssssssssssst	3
\.


--
-- Data for Name: booking; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.booking (id, annonce_id, tenant_id, created_at, start_at, nb_people, statut) FROM stdin;
1	1	61	2022-05-19 00:06:18	2022-05-19 09:00:00	3	5
\.


--
-- Data for Name: booking_slot; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.booking_slot (booking_id, slot_id) FROM stdin;
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
DoctrineMigrations\\Version20220108163723	2022-01-08 16:37:31	37
DoctrineMigrations\\Version20220108183520	2022-01-08 18:35:25	41
DoctrineMigrations\\Version20220327162519	2022-03-27 16:26:52	267
DoctrineMigrations\\Version20220423131846	2022-04-23 13:21:36	91
DoctrineMigrations\\Version20220503211744	2022-05-03 21:18:11	47
DoctrineMigrations\\Version20220503212034	2022-05-03 21:21:05	41
DoctrineMigrations\\Version20220512170039	2022-05-12 17:09:47	104
DoctrineMigrations\\Version20220512193718	2022-05-12 19:41:01	82
DoctrineMigrations\\Version20220109140407	2022-05-13 07:17:38	29
DoctrineMigrations\\Version20220513102503	2022-05-13 10:25:36	51
DoctrineMigrations\\Version20220514091709	2022-05-15 15:21:34	53
DoctrineMigrations\\Version20220517223437	2022-05-17 22:35:53	84
DoctrineMigrations\\Version20220517225302	2022-05-17 22:53:44	56
DoctrineMigrations\\Version20220519203651	2022-05-19 20:37:22	56
DoctrineMigrations\\Version20220519221248	2022-05-19 22:13:13	21
DoctrineMigrations\\Version20220519221653	2022-05-19 22:17:17	23
DoctrineMigrations\\Version20220521150022	2022-05-21 15:00:59	58
\.


--
-- Data for Name: files; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.files (id, user_id, name, annonce_id, type, principal, path, created_at) FROM stdin;
1	\N	3	1	jpeg	f	\\/path	\N
2	\N	41	2	jpeg	f	\\/path	\N
3	\N	37	2	jpeg	f	\\/path	\N
4	\N	28	2	jpeg	f	\\/path	\N
5	\N	téléchargement (13)	2	jpeg	f	\\/path	\N
6	\N	images (50)	3	jpeg	f	\\/path	\N
7	\N	11	3	jpeg	f	\\/path	\N
8	\N	22	3	jpeg	f	\\/path	\N
9	\N	téléchargement (21)	3	jpeg	f	\\/path	\N
10	\N	15	3	jpeg	f	\\/path	\N
11	\N	.	4		f	\\/path	\N
12	\N	ba0884daa7f03b912c8001bce5e041b7	5	jpg	f	\\/path	\N
13	\N	43	5	jpeg	f	\\/path	\N
14	\N	42	5	jpeg	f	\\/path	\N
15	\N	5	5	jpeg	f	\\/path	\N
16	\N	téléchargement	5	png	f	\\/path	\N
17	\N	téléchargement (19)	6	jpeg	f	\\/path	\N
18	\N	téléchargement (18)	6	jpeg	f	\\/path	\N
19	\N	téléchargement (7)	6	jpeg	f	\\/path	\N
20	\N	233a185191246cc675373fa1a9f6c514	6	jpg	f	\\/path	\N
21	\N	téléchargement (16)	6	jpeg	f	\\/path	\N
22	\N		7		f	\\/path	\N
23	\N	images (48)	8	jpeg	f	\\/path	\N
24	\N	téléchargement (24)	8	jpeg	f	\\/path	\N
25	\N	téléchargement (4)	8	jpeg	f	\\/path	\N
26	\N	téléchargement (27)	8	jpeg	f	\\/path	\N
27	\N	téléchargement (23)	9	jpeg	f	\\/path	\N
28	\N	5	9	jpeg	f	\\/path	\N
29	\N	233a185191246cc675373fa1a9f6c514	9	jpg	f	\\/path	\N
30	\N	images (47)	9	jpeg	f	\\/path	\N
31	\N	23	9	jpeg	f	\\/path	\N
32	\N	téléchargement (8)	10	jpeg	f	\\/path	\N
33	\N	téléchargement (11)	11	jpeg	f	\\/path	\N
34	\N	40	11	jpeg	f	\\/path	\N
35	\N	44	12	jpeg	f	\\/path	\N
36	\N	8	13	jpeg	f	\\/path	\N
37	\N	téléchargement (8)	13	jpeg	f	\\/path	\N
38	\N	images (45)	13	jpeg	f	\\/path	\N
39	\N	33	14	jpeg	f	\\/path	\N
40	\N	images (45)	14	jpeg	f	\\/path	\N
41	\N	37	14	jpeg	f	\\/path	\N
42	\N	42	14	jpeg	f	\\/path	\N
43	\N	1	15	jpeg	f	\\/path	\N
44	\N	14	15	jpeg	f	\\/path	\N
47	\N	test	\N	image/jpeg	t	627d67f360c8d_4.jpeg	2022-05-12 20:02:59
49	48	ui	\N	image/jpeg	t	627d898009b86_2.jpeg	2022-05-12 22:26:08
48	61	test2	\N	image/png	t	627d8ab4b4104_dataset-expression.png	2022-05-12 22:31:16
50	\N	test	1	image/jpeg	t	628d34d89bf46_3.jpeg	2022-05-24 19:41:12
51	\N	test	1	image/png	f	628e9cec23477_webby.png	2022-05-25 21:17:32
52	\N	test	1	image/png	f	628eaaa3d6747_webby.png	2022-05-25 22:16:03
53	\N	test	1	image/png	f	628eb2ff6f136_webby.png	2022-05-25 22:51:43
\.


--
-- Data for Name: payement; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.payement (id, booking_id, paypal_response, statut) FROM stdin;
1	1	{\n  "id": "3HX66825G1077980G",\n  "intent": "CAPTURE",\n  "status": "COMPLETED",\n  "purchase_units": [\n    {\n      "reference_id": "default",\n      "amount": {\n        "currency_code": "EUR",\n        "value": "15.00"\n      },\n      "payee": {\n        "email_address": "sb-c7eix15183450@business.example.com",\n        "merchant_id": "94VZK7VCS4RXE"\n      },\n      "shipping": {\n        "name": {\n          "full_name": "antoine chabreuil"\n        },\n        "address": {\n          "address_line_1": "Av. de la Pelouse",\n          "admin_area_2": "Paris",\n          "admin_area_1": "Alsace",\n          "postal_code": "75002",\n          "country_code": "FR"\n        }\n      },\n      "payments": {\n        "captures": [\n          {\n            "id": "2JN16869N55896302",\n            "status": "COMPLETED",\n            "amount": {\n              "currency_code": "EUR",\n              "value": "15.00"\n            },\n            "final_capture": true,\n            "disbursement_mode": "INSTANT",\n            "seller_protection": {\n              "status": "ELIGIBLE",\n              "dispute_categories": [\n                "ITEM_NOT_RECEIVED",\n                "UNAUTHORIZED_TRANSACTION"\n              ]\n            },\n            "create_time": "2022-05-19T21:09:49Z",\n            "update_time": "2022-05-19T21:09:49Z"\n          }\n        ]\n      }\n    }\n  ],\n  "payer": {\n    "name": {\n      "given_name": "antoine",\n      "surname": "chabreuil"\n    },\n    "email_address": "sb-mnwa116418614@personal.example.com",\n    "payer_id": "J8XEVKTQKF5LW",\n    "address": {\n      "address_line_1": "Av. de la Pelouse",\n      "admin_area_2": "Paris",\n      "admin_area_1": "Alsace",\n      "postal_code": "75002",\n      "country_code": "FR"\n    }\n  },\n  "create_time": "2022-05-19T21:09:15Z",\n  "update_time": "2022-05-19T21:09:49Z",\n  "links": [\n    {\n      "href": "https://api.sandbox.paypal.com/v2/checkout/orders/3HX66825G1077980G",\n      "rel": "self",\n      "method": "GET"\n    }\n  ]\n}	5
\.


--
-- Data for Name: paypal_information; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.paypal_information (id, owner_id, email, principal) FROM stdin;
1	61	sb-mnwa116418614@personal.example.com	t
2	62	test@test.com	f
\.


--
-- Data for Name: refund; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.refund (id, booking_id, statut, paypal_response) FROM stdin;
1	1	4	{"id":"6UG80685X5804564E","state":"completed","amount":{"total":"15.00","currency":"EUR"},"refund_from_received_amount":{"value":"15.00","currency":"EUR"},"refund_from_transaction_fee":{"value":"0.00","currency":"EUR"},"total_refunded_amount":{"value":"15.00","currency":"EUR"},"parent_payment":"PAYID-MKDLEHI683576919R915540V","sale_id":"2JN16869N55896302","create_time":"2022-05-19T21:10:33Z","update_time":"2022-05-19T21:10:33Z","links":[{"href":"https:\\/\\/api.sandbox.paypal.com\\/v1\\/payments\\/refund\\/6UG80685X5804564E","rel":"self","method":"GET"},{"href":"https:\\/\\/api.sandbox.paypal.com\\/v1\\/payments\\/payment\\/PAYID-MKDLEHI683576919R915540V","rel":"parent_payment","method":"GET"},{"href":"https:\\/\\/api.sandbox.paypal.com\\/v1\\/payments\\/sale\\/2JN16869N55896302","rel":"sale","method":"GET"}]}
\.


--
-- Data for Name: slot; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public.slot (id, annonce_id, start_date, end_date, price) FROM stdin;
2	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
3	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
4	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
5	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
6	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
7	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
8	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
9	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
10	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
11	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
12	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
13	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
14	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
15	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
16	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
17	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
18	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
19	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
20	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
21	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
22	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
23	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
24	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
25	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
26	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
27	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
28	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
29	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
30	1	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
31	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
32	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
33	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
34	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
35	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
36	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
37	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
38	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
39	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
40	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
41	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
42	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
43	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
44	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
45	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
46	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
47	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
48	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
49	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
50	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
51	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
52	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
53	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
54	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
55	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
56	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
57	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
58	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
59	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
60	2	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
61	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
62	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
63	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
64	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
65	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
66	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
67	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
68	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
69	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
70	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
71	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
72	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
73	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
74	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
75	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
76	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
77	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
78	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
79	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
80	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
81	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
82	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
83	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
84	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
85	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
86	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
87	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
88	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
89	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
90	3	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
91	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
92	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
93	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
94	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
95	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
96	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
97	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
98	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
99	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
100	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
101	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
102	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
103	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
104	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
105	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
106	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
107	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
108	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
109	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
110	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
111	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
112	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
113	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
114	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
115	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
116	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
117	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
118	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
119	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
120	4	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
121	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
122	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
123	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
124	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
125	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
126	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
127	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
128	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
129	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
130	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
131	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
132	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
133	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
134	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
135	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
136	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
137	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
138	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
139	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
140	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
141	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
142	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
143	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
144	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
145	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
146	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
147	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
148	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
149	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
150	5	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
151	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
152	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
153	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
154	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
155	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
156	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
157	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
158	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
159	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
160	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
161	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
162	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
163	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
164	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
165	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
166	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
167	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
168	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
169	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
170	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
171	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
172	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
173	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
174	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
175	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
176	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
177	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
178	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
179	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
180	6	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
181	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
182	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
183	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
184	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
185	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
186	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
187	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
188	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
189	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
190	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
191	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
192	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
193	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
194	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
195	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
196	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
197	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
198	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
199	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
200	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
201	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
202	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
203	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
204	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
205	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
206	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
207	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
208	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
209	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
210	7	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
211	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
212	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
213	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
214	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
215	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
216	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
217	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
218	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
219	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
220	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
221	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
222	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
223	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
224	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
225	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
226	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
227	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
228	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
229	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
230	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
231	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
232	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
233	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
234	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
235	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
236	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
237	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
238	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
239	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
240	8	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
241	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
242	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
243	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
244	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
245	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
246	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
247	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
248	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
249	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
250	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
251	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
252	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
253	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
254	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
255	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
256	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
257	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
258	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
259	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
260	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
261	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
262	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
263	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
264	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
265	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
266	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
267	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
268	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
269	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
270	9	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
271	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
272	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
273	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
274	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
275	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
276	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
277	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
278	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
279	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
280	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
281	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
282	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
283	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
284	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
285	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
286	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
287	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
288	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
289	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
290	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
291	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
292	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
293	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
294	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
295	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
296	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
297	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
298	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
299	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
300	10	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
301	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
302	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
303	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
304	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
305	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
306	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
307	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
308	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
309	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
310	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
311	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
312	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
313	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
314	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
315	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
316	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
317	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
318	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
319	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
320	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
321	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
322	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
323	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
324	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
325	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
326	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
327	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
328	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
329	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
330	11	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
331	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
332	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
333	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
334	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
335	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
336	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
337	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
338	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
339	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
340	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
341	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
342	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
343	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
344	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
345	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
346	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
347	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
348	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
349	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
350	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
351	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
352	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
353	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
354	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
355	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
356	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
357	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
358	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
359	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
360	12	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
361	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
362	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
363	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
364	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
365	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
366	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
367	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
368	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
369	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
370	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
371	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
372	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
373	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
374	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
375	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
376	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
377	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
378	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
379	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
380	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
381	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
382	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
383	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
384	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
385	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
386	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
387	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
388	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
389	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
390	13	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
391	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
392	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
393	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
394	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
395	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
396	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
397	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
398	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
399	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
400	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
401	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
402	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
403	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
404	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
405	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
406	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
407	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
408	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
409	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
410	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
411	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
412	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
413	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
414	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
415	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
416	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
417	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
418	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
419	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
420	14	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
421	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
422	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
423	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
424	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
425	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
426	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
427	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
428	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
429	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
430	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
431	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
432	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
433	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
434	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
435	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
436	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
437	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
438	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
439	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
440	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
441	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
443	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
444	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
445	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
446	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
447	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
448	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
449	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
450	15	2023-04-23 10:00:00	2023-04-24 09:00:00	\N
442	15	2023-04-18 10:00:00	2023-04-19 09:00:00	\N
1	1	2023-04-23 10:00:00	2023-04-24 09:00:00	14
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: api-platform
--

COPY public."user" (id, email, roles, password, first_name, last_name, pseudo, active, token) FROM stdin;
31	aurore21@devaux.fr	["ROLE_USER"]	$2y$13$sDG3LR7lleh8fromZXiqu.9SYT6VIyT1LXX3mS.NdnIQA.7lBIdq2	Alphonse	Lesage	oceane.ollivier	t	\N
32	anastasie59@tiscali.fr	["ROLE_USER"]	$2y$13$/c7MZuac5HVjnfQD16e4OOhK4VyFnno/MAFzGeHzKQZwi8NmwWp2a	Guillaume	Herve	virginie74	t	\N
33	ylegall@sfr.fr	["ROLE_USER"]	$2y$13$af2KHPuXAbHJXgodyA8eJeFaG4D0hzDb16vBYrf7hR/UPQs03WIr6	Jules	Begue	thomas.stephane	t	\N
34	lemonnier.olivier@carpentier.fr	["ROLE_USER"]	$2y$13$U5ki/2ICBudRL5FUWSSJj.R43hPPY4VuUWE7Mk71KziLCCbr1s7O.	Anne	Guillaume	henriette.martinez	t	\N
35	philippe.adam@legall.fr	["ROLE_USER"]	$2y$13$HTAUnHiF2drvC0POPoItau0wZfLj3rW4E3TscBG0zpE2PW3AI6VF6	Honoré	Simon	rossi.bernadette	t	\N
36	victoire.martineau@lesage.fr	["ROLE_USER"]	$2y$13$hVQHJ0gLN8wnIZ.iwKpT8OM/4fRH97SalBpTGDHPVFxTqATVgeqLG	Honoré	Launay	gabriel.marques	t	\N
37	adrienne.denis@noos.fr	["ROLE_USER"]	$2y$13$6YVOP1Q9654IBArodQG/Tu/fOTPEOLjF55O8ZMhuBkOEnHw9rodni	Nathalie	Guillet	noel86	t	\N
38	potier.andre@lejeune.net	["ROLE_USER"]	$2y$13$H40mEFihWoWu3nWIXN9o/uJTfCkmdYGZDQM//8DknugDWiQ78mHuq	Richard	Launay	valentin.dorothee	t	\N
39	joseph14@free.fr	["ROLE_USER"]	$2y$13$44qC7RVuoNMSl1l6kllDR.oZQY6skfSh389mxHAext9Vi7DY.vV1K	Édouard	Bertin	paul.martel	t	\N
40	jourdan.benjamin@chauveau.com	["ROLE_USER"]	$2y$13$bI/LwyM1eHw54SBVSKzCLebKOIToDSz.rqVp8u.ytDKRNOWDcX14O	Roger	Bernard	marthe81	t	\N
41	nath93@salmon.fr	["ROLE_USER"]	$2y$13$o1dNYiEj2o3h.zBTH2M8NuuU7VtvUONzZK3CcFYFDLrD/LF1GhDze	Diane	De Sousa	operrin	t	\N
42	bboulay@voila.fr	["ROLE_USER"]	$2y$13$pY3wdiuwFBUlywoG24946OrWyPe4Bk7dEmbwG88GvXc3ZVStDnkt2	Margot	Blanc	fparent	t	\N
43	tleroux@roy.com	["ROLE_USER"]	$2y$13$cz/EcHKHyB7RGhY.KZYlzO7C6WcX.J03J3TQlsh0yggIgnVt8lgca	Océane	Guillot	auguste63	t	\N
44	theophile21@live.com	["ROLE_USER"]	$2y$13$YwoxougiF9.thtI24w.V6OgppuIxdTqF8dxMvsezA6FC1Z44sCW4.	Marie	Courtois	moreau.jean	t	\N
45	jeanne79@riviere.fr	["ROLE_USER"]	$2y$13$7Q8V03vmSl04O5kEYGNwpO8uc6AmUP5/OV0k/f.CUpKfjnG..NJW2	Jeannine	Brunet	manon.gillet	t	\N
46	umarechal@leduc.fr	["ROLE_USER"]	$2y$13$f7P0sKno7QiRn6b.9Dc.5O8cAJSs4JDsH/11EUe37CKnfwWrXLunC	Maggie	Guillou	rousseau.pauline	t	\N
47	letellier.simone@brunel.fr	["ROLE_USER"]	$2y$13$QcBKm8jF9pR2CCN1SSm2C.6dugVZdREGu3wbIHsnyDnMSylCvursa	Élise	Barthelemy	bchevalier	t	\N
48	gosselin.corinne@antoine.fr	["ROLE_USER"]	$2y$13$lXtjRJDDU4ZUxwWbs7H4I.35YgR/68cPLKMkQbLnVgDvtOdquYFGe	Luce	Alves	cvincent	t	\N
49	suzanne.lefevre@noos.fr	["ROLE_USER"]	$2y$13$Npm8eDtZV7QL5SKBGlZ3xOVZzDIoc/1JZqxDAh1ZjRkfGXldrQKUG	Camille	Tessier	lblanchet	t	\N
50	tgregoire@guillot.fr	["ROLE_USER"]	$2y$13$mDMVdykRMnkEbIFpTjE3HefIrO9m5POZLEpVFBT/pRvJSEMJJlZxq	Benjamin	Tessier	guilbert.auguste	t	\N
51	odescamps@martins.com	["ROLE_USER"]	$2y$13$ESOqv1IpJxW8nangA04OQeoXF.NKZkWdR0AHMRRvyd1qjzLXiXJba	Victor	Vallee	xweber	t	\N
52	rdumont@voila.fr	["ROLE_USER"]	$2y$13$mnI07WxS3O.NmzLDqazl/eP./38gePhs8B1b2EkkEJxID9yJP8ex2	Philippe	Bonneau	alix.lemoine	t	\N
53	gregoire.pereira@turpin.com	["ROLE_USER"]	$2y$13$CzLJIHseJl8fTqsQ3hAx6OChKX6i8wPnerJ/4vjmSaZK9kFBuu9se	Yves	Caron	jules89	t	\N
54	ruiz.theodore@wanadoo.fr	["ROLE_USER"]	$2y$13$7S/a8tdmi/w.9ZM.SshE4.rd4yWTd35xZTA1EVz3ARiugI1GYQn8i	Benoît	Huet	colette.didier	t	\N
55	boutin.rene@bouygtel.fr	["ROLE_USER"]	$2y$13$AM4XDGc8ROvNQ5hqmkjV7uNshdxRRewdjf4Fl157VYeimpPltQE4i	Cécile	Lecomte	roger74	t	\N
56	leon.barthelemy@regnier.fr	["ROLE_USER"]	$2y$13$lvnNswJp.Aqjn0U6eqR.c.BsWUg.k71Kps2k6QSktZQPmDdNve6eu	Léon	Lemonnier	tcarpentier	t	\N
57	laroche.andree@roux.com	["ROLE_USER"]	$2y$13$LDibctVynTsIecWZToVljepf73.LoKMTPzZf97e8ostZ8GNbgc5Ca	Madeleine	Fleury	whoareau	t	\N
58	josephine46@blanchard.net	["ROLE_USER"]	$2y$13$EjRIGOd9Lvg/qkl69vqg3eazvzZ1DBsIQD2AIUckgaKxSVS8dJUjC	François	Vasseur	philippe.reynaud	t	\N
59	labbe.jeannine@gmail.com	["ROLE_USER"]	$2y$13$TBVXRcXGgGNpgLHsYBcJyeI7OuTfBoy/Om2uNi6wT3oIDXCKKIQsC	Charlotte	Rodriguez	rene45	t	\N
60	claudine.guyot@etienne.org	["ROLE_USER"]	$2y$13$TIjI0mV54f8PL5A/n9W12eaQVNFNSs7XaBXn.IxjHZm7uaqctCJbK	Aurélie	Gay	alix24	t	\N
61	antoinechabreuil@gmail.com	["ROLE_SUPER_ADMIN"]	$2y$13$wO2iDtmEYQWOFsaJaZBI4OPyR1D4gRkOLChEIJA31zQvVdsfo9mRK	antoine	chabreuil	antoine	t	\N
62	antoinechabreuil2@gmail.com	["ROLE_USER"]	$2y$13$xB2RN/QGlgj2k7tU3Byyyuhzd7T.pJFbEaAwOJdXuezqjZgr1GhZm	antoine	chabreuil	achab	t	\N
\.


--
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.address_id_seq', 32, true);


--
-- Name: annonce_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.annonce_id_seq', 18, true);


--
-- Name: booking_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.booking_id_seq', 1, false);


--
-- Name: craue_geo_postalcode_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.craue_geo_postalcode_id_seq', 1, false);


--
-- Name: files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.files_id_seq', 53, true);


--
-- Name: payement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.payement_id_seq', 2, true);


--
-- Name: paypal_information_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.paypal_information_id_seq', 2, true);


--
-- Name: refund_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.refund_id_seq', 1, true);


--
-- Name: slot_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.slot_id_seq', 450, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: api-platform
--

SELECT pg_catalog.setval('public.user_id_seq', 62, true);


--
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- Name: annonce annonce_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.annonce
    ADD CONSTRAINT annonce_pkey PRIMARY KEY (id);


--
-- Name: booking booking_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.booking
    ADD CONSTRAINT booking_pkey PRIMARY KEY (id);


--
-- Name: booking_slot booking_slot_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.booking_slot
    ADD CONSTRAINT booking_slot_pkey PRIMARY KEY (booking_id, slot_id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: files files_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- Name: payement payement_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.payement
    ADD CONSTRAINT payement_pkey PRIMARY KEY (id);


--
-- Name: paypal_information paypal_information_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.paypal_information
    ADD CONSTRAINT paypal_information_pkey PRIMARY KEY (id);


--
-- Name: refund refund_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.refund
    ADD CONSTRAINT refund_pkey PRIMARY KEY (id);


--
-- Name: slot slot_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.slot
    ADD CONSTRAINT slot_pkey PRIMARY KEY (id);


--
-- Name: user user_pkey; Type: CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- Name: idx_63540598805ab2f; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_63540598805ab2f ON public.files USING btree (annonce_id);


--
-- Name: idx_6354059a76ed395; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_6354059a76ed395 ON public.files USING btree (user_id);


--
-- Name: idx_ac0e20678805ab2f; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_ac0e20678805ab2f ON public.slot USING btree (annonce_id);


--
-- Name: idx_b20a78853301c60; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_b20a78853301c60 ON public.payement USING btree (booking_id);


--
-- Name: idx_b49f02293301c60; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_b49f02293301c60 ON public.booking_slot USING btree (booking_id);


--
-- Name: idx_b49f022959e5119c; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_b49f022959e5119c ON public.booking_slot USING btree (slot_id);


--
-- Name: idx_d7fbc1cb7e3c61f9; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_d7fbc1cb7e3c61f9 ON public.paypal_information USING btree (owner_id);


--
-- Name: idx_e00cedde8805ab2f; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_e00cedde8805ab2f ON public.booking USING btree (annonce_id);


--
-- Name: idx_e00cedde9033212a; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_e00cedde9033212a ON public.booking USING btree (tenant_id);


--
-- Name: idx_f65593e5e289a545; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE INDEX idx_f65593e5e289a545 ON public.annonce USING btree (renter_id);


--
-- Name: uniq_5b2c14583301c60; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE UNIQUE INDEX uniq_5b2c14583301c60 ON public.refund USING btree (booking_id);


--
-- Name: uniq_8d93d649e7927c74; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE UNIQUE INDEX uniq_8d93d649e7927c74 ON public."user" USING btree (email);


--
-- Name: uniq_f65593e5f5b7af75; Type: INDEX; Schema: public; Owner: api-platform
--

CREATE UNIQUE INDEX uniq_f65593e5f5b7af75 ON public.annonce USING btree (address_id);


--
-- Name: refund fk_5b2c14583301c60; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.refund
    ADD CONSTRAINT fk_5b2c14583301c60 FOREIGN KEY (booking_id) REFERENCES public.booking(id);


--
-- Name: files fk_63540598805ab2f; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT fk_63540598805ab2f FOREIGN KEY (annonce_id) REFERENCES public.annonce(id);


--
-- Name: files fk_6354059a76ed395; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT fk_6354059a76ed395 FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- Name: slot fk_ac0e20678805ab2f; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.slot
    ADD CONSTRAINT fk_ac0e20678805ab2f FOREIGN KEY (annonce_id) REFERENCES public.annonce(id);


--
-- Name: payement fk_b20a78853301c60; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.payement
    ADD CONSTRAINT fk_b20a78853301c60 FOREIGN KEY (booking_id) REFERENCES public.booking(id);


--
-- Name: booking_slot fk_b49f02293301c60; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.booking_slot
    ADD CONSTRAINT fk_b49f02293301c60 FOREIGN KEY (booking_id) REFERENCES public.booking(id) ON DELETE CASCADE;


--
-- Name: booking_slot fk_b49f022959e5119c; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.booking_slot
    ADD CONSTRAINT fk_b49f022959e5119c FOREIGN KEY (slot_id) REFERENCES public.slot(id) ON DELETE CASCADE;


--
-- Name: paypal_information fk_d7fbc1cb7e3c61f9; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.paypal_information
    ADD CONSTRAINT fk_d7fbc1cb7e3c61f9 FOREIGN KEY (owner_id) REFERENCES public."user"(id);


--
-- Name: booking fk_e00cedde8805ab2f; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.booking
    ADD CONSTRAINT fk_e00cedde8805ab2f FOREIGN KEY (annonce_id) REFERENCES public.annonce(id);


--
-- Name: booking fk_e00cedde9033212a; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.booking
    ADD CONSTRAINT fk_e00cedde9033212a FOREIGN KEY (tenant_id) REFERENCES public."user"(id);


--
-- Name: annonce fk_f65593e5e289a545; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.annonce
    ADD CONSTRAINT fk_f65593e5e289a545 FOREIGN KEY (renter_id) REFERENCES public."user"(id);


--
-- Name: annonce fk_f65593e5f5b7af75; Type: FK CONSTRAINT; Schema: public; Owner: api-platform
--

ALTER TABLE ONLY public.annonce
    ADD CONSTRAINT fk_f65593e5f5b7af75 FOREIGN KEY (address_id) REFERENCES public.address(id);


--
-- PostgreSQL database dump complete
--

